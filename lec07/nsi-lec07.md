---
marp: true
paginate: true

style: |
    img[alt~="right"] {
      position: relative;
      top: -120px;
      float: right;
    }
    img[alt~="center"] {
      display: block;
      margin: 0 auto;
      padding-top: 20px; 
    }
    section{
      
    }
    strong {color: #455a64; font-weight: 600;}

    div.twocols {
        margin-top: 35px;
        column-count: 2;
    }
    div.twocols p:first-child,
    div.twocols h1:first-child,
    div.twocols h2:first-child,
    div.twocols ul:first-child,
    div.twocols ul li:first-child,
    div.twocols ul li p:first-child {
        margin-top: 0 !important;
    }
    div.twocols p.break {
        break-before: column;
        margin-top: 0;
    }
---

<!-- _paginate: false -->

# Návrh systémů IoT

## 7. Bezdrátové sítě pro IoT

Stanislav Vítek
Katedra radioelektroniky
České vysoké učení technické v Praze

---

# Obsah přednášky

[Fyzická a linková vstva](#6)
[Domácí a průmyslová automatizace](#39)
[Síťová vrstva pro IoT](#46)

---

# Počítačové sítě

**Síť:** skupina počítačů a přidružených zařízení, která jsou propojena komunikačními prostředky.

* Wide Area Network **WAN**: celosvětová síť (internet)
* Metropolitní síť **MAN**: městská síť
* Local Area Network **LAN**: laboratorní/kancelářská síť (Ethernet)
  * **WLAN:** bezdrátová síť LAN (Wi-Fi)
  * **WPAN:** bezdrátová osobní síť (Bluetooth)
  * **WBAN:** bezdrátová síť v oblasti těla

---

# Topologie počítačových sítí

![h:580px center](figs/iot-01.png)

---

# IoT OSI (Open System Interconnection)

![h:550px center](figs/iot-11.png)

---

# Fyzická a linková vrstva

---

# Protokoly fyzické a linkové vrstvy

* Tyto protokoly jsou základními stavebními kameny síťové architektury.
* Formální standardy a zásady umožňující komunikaci
* Standardizace: **IEEE** (Institute of Electrical and Electronics Engineers)
* Příklad: Projekt 802
  * **802.3** Ethernet
  * **802.11** WLAN (WiFi)
  * **802.15.1** WPAN (Bluetooth)
  * **802.15.4** LR-WPAN (Low-rate WPAN, ZigBee)
  * **802.16** WMAN (WiMAX)

---

# Parametry sítí

![h:600px center](figs/iot-000.png)

---

# Výzvy spojené s rozsáhlými IoT systémy

* **Životnost baterií** - mnoho IoT zařízení je napájené bateriemi a cena výměny baterií není únosná 
* **Pokrytí a dosah** - regionální (nebo ještě lépe národní/mezinárodní) pokrytí službami je zásadní pro řadu aplikací. Pro řadu systémů je důležitá i možnost připojení ke službě uvnitř budov
* **Kvalita služby (QoS)** - klíčová je přijatelná kvalita přenosu s nízkým bitovým tokem, odezva (latence) nebývá kritickým parametrem
* **Deployment a ceny**

---

# Wi-Fi

* Využívá 2 frekvenční pásma (802.11 ...): 2,4 GHz a 5 GHz
  * neomezené použití, jediným omezením je výkon TX

* Mírně odlišné spektrum v různých zemích

* Jádro standardu obsahuje 14 kanálů pro pásmo 2,4 GHz

* Šířka pásma pro každý kanál byla původně 20 MHz, nyní je to 40 MHz.

* Přenosová rychlost někde mezi 2Mb/s a několika Gb/s (nejnovější ac)

**Poznámka:** neexistuje žádný centralizovaný řadič pro řízení přístupu k PHY, takže často dochází ke kolizím -- nutně dochází ke snižování propustnosti sítě při rostoucím počtu zařízení na stejné síti/frekvenci.
* Obvyklá konfigurace je jeden centrální směrovač/přepínač (AP) a připojené klienty. 

---

# Bluetooth

* Začal projektem Bluetooth společnosti Ericsson v roce 1994 pro rádiovou komunikaci mezi mobilními telefony na krátké vzdálenosti.
* Pojmenován po dánském králi Heraldu Blatandovi (940-981 n.l.), který měl rád borůvky.
* Intel, IBM, Nokia, Toshiba a Ericsson založily v květnu 1998 sdružení [Bluetooth SIG](https://en.wikipedia.org/wiki/Bluetooth_Special_Interest_Group).
* Koncem roku 1999 vyšla verze 1.0A specifikace.
* IEEE 802.15.1 schválená počátkem roku 2002 je založena na Bluetooth.
* Klíčové funkce:
  * nízká spotřeba: 10 mA v pohotovostním režimu, 50 mA při vysílání
  * levné: 5 USD za zařízení
  * Malé: čipy o velikosti 9 $mm^2$

---

# Verze Bluetooth 1/2

* **Bluetooth 1.2:** IEEE 802.15.1-2005 (listopad 2003) 
  * Rozšířený SCO (Synchronous Connection Oriented Link)
  * Vyšší proměnlivá rychlost retranslace pro SCO
  * Adaptivní [frekvency hopping](https://docs.silabs.com/bluetooth/2.13/general/system-and-performance/adaptive-frequency-hopping)
* **Bluetooth 2.0** (listopad 2004) 
  * Enhanced Data Rate (EDR)
  * 3 Mb/s pomocí DPSK. 
  * Pro video aplikace. 
* **Bluetooth 2.1** (červenec 2007) 
  * Secure Simple Pairing pro urychlení párování.

---

# Verze Bluetooth

* **Bluetooth 3.0** (duben 2009)
  * High Speed (HS)
  * 24 Mb/s pomocí WiFi PHY + Bluetooth PHY pro nižší rychlosti
* **Bluetooth 4.0** (červen 2010)
  * Nízkoenergetický, Bluetooth Smart (preferováno SIG) nebo BLE
  * Menší zařízení vyžadující delší životnost baterie (několik let). 
  * Nová nekompatibilní PHY
* Bluetooth 4.1: 4.0 + změny základní specifikace (CSA) 1, 2, 3, 4.
* **Bluetooth 4.2** (prosinec 2014)
  * Větší pakety, zabezpečení/soukromí, profil IPv6

---

# Bluetooth vlastnosti

* Standard: 7+1 zařízení (slaves + master)
  * Pikosítě se však mohou překrývat a vytvářet rozptýlené sítě
  * Specifikace umožňuje až spojení až 10 pikosítí na ploše o průměru 10m
  * Jedno zařízení musí fungovat jako **master** pikosítě - řídí celou síť
* 2,4 GHz, sdílené s WiFi
  * 79 kanálů - automatická změna kanálu (frequency hopping)
  * 1 kanál podporuje rychlost až 1 Mb/s, přibližně 700 kb/s pro uživatele
* Nové čipy až 2-3Mbps, teoreticky na 100 m (Enhanced Data Rate, EDR)
  * Třída 1 - 100mW
  * Třída 2 - 2,5 mW
  * Třída 3 - 1mW
* Každé zařízení má adresu MAC (48 bitů)
<!--* Existují různé "profily" (jako služby), které mají mají vliv na konstrukci Bluetooth stacku, zařízení obvykle neimplementují všechny profily -->

---

# Bluetooth topologie

![h:600px center](figs/iot-09.png)

---

# Bluetooth 4.0 / BLE / Smart

* Nízká spotřeba energie: 1 % až 50 % klasické technologie Bluetooth
* Pro krátké vysílání: Ne pro hlasové vysílání, přenosy souborů, ...
* Malé zprávy: Rychlost přenosu dat 1 Mb/s, ale propustnost není kritická.
* Životnost baterie: knoflíková baterie vydží roky
* Jednoduchost: Hvězdicová topologie. Žádné rozptýlené sítě, mesh, ...
* Nižší náklady než u klasického Bluetooth
* Nový design protokolu založený na technologii WiBree společnosti Nokia 
  * Sdílí stejné 2,4GHz rádio jako Bluetooth
  * Čipy s duálním režimem
* Všechny nové chytré telefony (iPhone, Android, ...) mají čipy s duálním režimem.

---

# Bluetooth Smart - fyzická vrstva

* 2,4 GHz. Otevřené pole 150 m

* Hvězdicová topologie

* 1 Mb/s GFSK, lepší dosah než klasický Bluetooth

* Adaptivní přeskakování frekvence. 40 kanálů s rozestupem 2 MHz

* 3 kanály vyhrazené pro advertising a 37 kanálů pro data.
* Advertising kanály speciálně vybrané tak, aby se zabránilo rušení s Wi-Fi kanály

---

# Bluetooth Smart - MAC (Media Access Control)

* Dva typy zařízení: **periferní zařízení** jednodušší než **centrální zařízení**

* Dva typy PDU (Protocol Data Unit): advertisement, data

* [Advertisement](https://docs.silabs.com/bluetooth/4.0/general/adv-and-scanning/bluetooth-adv-data-basics)
  * **Non-connectable** - pouze vysílání (broadcast)
  * **Discoverable** - centrální zařízení může požadovat další informace. Periferní zařízení může odesílat data bez připojení
  * **General** - periferie, která se chce připojit, sděluje svoji přítomnost. Centrála si může vyžádat krátké spojení.
  * **Directed** - vysílání podepsaných dat do dříve připojené centrály

---

# Bluetooth Smart Protocol Stack

![h:550px center](figs/iot-16.png)

---

# Generic Attribute Profile - GATT

![h:550px center](figs/iot-17.png)

---

# GATT operace

* Centrální zařízení může
  * zjistit UUID pro všechny primární služby
  * vyhledat službu s daným UUID
  * vyhledat sekundární služby pro danou primární službu
  * zjistit všechny charakteristiky pro danou službu
  * vyhledat charakteristiky odpovídající danému UUID
  * čtení všech deskriptorů pro danou charakteristiku
  * může provádět čtení, zápis, dlouhé čtení, dlouhý zápis hodnot atd.

* Periferní zařízení může
  * Oznamovat nebo indikovat centrálnímu zařízení změny

---

# Bluetooth Smart aplikace

* Blízkost: v autě, V pokoji 303, V nákupním centru

* Lokátor: klíče, hodinky, zvířata

* Zdravotní zařízení: snímač srdečního tepu, monitory tělesných aktivit, teploměr

* Senzory: teplota, stav baterie, tlak v pneumatikách

* Dálkové ovládání: otevírání/zavírání zámků, zapínání světel

---

# IEEE 802.15.4 LR-WPAN (ZigBee)}
	
* Technologie ZigBee je jednodušší (a levnější) než Bluetooth.

* Hlavním cílem LR-WPAN, jako je ZigBee, je 
  * snadná instalace, 
  * spolehlivý přenos dat, 
  * provoz na krátkou vzdálenost, 
  * extrémně nízké náklady, 
  * možnost připojení k síti přiměřená životnost baterií při zachování jednoduchého a flexibilního protokolu.
		
* Přibližná rychlost přenosu dat je dostatečně vysoká (maximálně 250 kbit/s) pro náročnější aplikace jako jsou interaktivní hračky, ale je také škálovatelná až na úroveň pro potřeby senzorů a automatizace (20 kbit/s nebo méně).

---

# ZigBee zařízení

* Sítě LR-WPAN se mohou účastnit dva různé typy zařízení:

  * Plně funkční zařízení (**FFD**, Full-function devices) mohou pracovat ve třech režimech: jako koordinátor osobní sítě (PAN), router nebo zařízení.
  
  * Zařízení s omezenou funkcí (**RFD**, Reduced-function devices) jsou určena pro aplikace, které jsou extrémně jednoduché.
  
* Zařízení **FFD** může komunikovat se zařízeními **RFD** nebo jinými zařízeními **FFD**.

* Zařízení **RFD** může komunikovat pouze se zařízením **FFD**.

--- 

# WPAN síť

* Dvě nebo více zařízení komunikujících na stejném fyzickém kanálu tvoří síť **WPAN**. 

* Síť **WPAN** musí obsahovat alespoň jedno **FFD**, které funguje jako koordinátor **PAN**.

* Koordinátor **PAN** zahajuje, ukončuje nebo směruje komunikaci v síti. 

* Koordinátor **PAN** je primární řídicí jednotkou sítě **PAN**.

---

# WPAN topologie

* Hvězdicová

  * Po první aktivaci **FFD** může vytvořit vlastní síť a stát se koordinátorem **PAN**.
  * Koordinátor **PAN** může ostatním zařízením povolit připojit ke své síti.

* Peer-to-peer

  * V síti peer-to-peer je každé **FFD** je schopen komunikovat s jakoukoli s jiným **FFD** v rámci své rádiové sféry vlivu. 
  * Jedno **FFD** bude nominováno jako koordinátor **PAN**.
  * Síť typu peer-to-peer může být ad hoc, samoorganizující se a samoopravující se, a může kombinovat zařízení pomocí síťové topologie mesh.

---

# Parametry ZigBee

* **Topologie** Ad-hoc (centrální PAN koordinátor)
* **RF pásmo** 2,4 MHz, 16 kanálů s rozestupy 5 MHz
* **Modulace** [Offset QPSK](https://cs.wikipedia.org/wiki/Kl%C3%AD%C4%8Dov%C3%A1n%C3%AD_f%C3%A1zov%C3%BDm_posuvem#Offset_QPSK_(OQPSK))
* **Přístup** [CSMA/CA](https://cs.wikipedia.org/wiki/CSMA/CA)
	
## CSMA/CA

* Pokaždé, když chce zařízení přenést datové rámce nebo příkazy MAC, musí počkat po náhodnou dobu. Pokud se zjistí, že kanál je nečinný, po uplynutí této doby zařízení přenese svá data. Pokud je zjištěno, že kanál je obsazený, zařízení vyčká náhodnou dobu, než se znovu pokusí získat přístup ke kanálu.
			
* Potvrzovací rámce se posílají bez použití mechanismu CSMA-CA.

---

# Zigbee stack

![h:500px center](figs/iot-001.png)

---

# WirelessHART

* Komunikační protokol [HART](https://en.wikipedia.org/wiki/Highway_Addressable_Remote_Transducer_Protocol) (Highway Addressable Remote Transducer Protocol) je určen k přidávání diagnostických informací do procesních zařízení kompatibilních se staršími analogovými přístroji 2-20mA.

* Celkový výkon byl navržen tak, aby vyhovoval potřebám automatizace procesů. Je schopen pracovat na vzdálenost až 1500 m.

* [WirelessHART](https://en.wikipedia.org/wiki/WirelessHART) je rozšířením protokolu HART, mezi jeho funkce patří např.
  * implementuje RF samoregenerační síť mesh
  * umožňuje synchronizaci času v rámci celé sítě
  * vylepšuje publikování/odebírání zpráv
  * dodává síťovou a transportní vrstvu
  * dodává rychlou linku pro časově kritický provoz a šifrování.

---

# Příklad sítě WirelessHART

![h:550px center](figs/iot-002.png)

---

# Charakteristiky WirelessHART

* Nízká spotřeba energie a levná zařízení

* Rychlost přenosu dat 250 kb/s na kanál v pásmu ISM 2,4 GHz s 15 kanály
* Založeno na vrstvě PHY standardu IEEE 802.15.4-2006.
* Založeno na vlastní vrstvě datového spoje s TDMA a CSMA/CA
* Podpora přeskakování kanálů a vytváření černých listin kanálů
* Síťová vrstva implementující samoregenerační síť typu mesh
* Aplikační vrstva plně kompatibilní s HART

---

# Architektura WirelessHART

* **Field devices** Patří mezi ně bezdrátové procesní převodníky a bezdrátové adaptéry HART.

* **Gateway** Brána propojuje bezdrátovou síť HART s kabelovou infrastrukturou.

* **Network manager (pouze jeden)** Je zodpovědný za konfiguraci sítě, komunikaci mezi zařízeními, správu směrování zpráv a monitorování stavu sítě.

* **Security manager** Správce zabezpečení se zabývá zabezpečením a šifrováním, nastavením klíčů relací a jejich pravidelnou změnou.

* **Handhold zařízení** pro účely údržby jsou volitelná.

---

# Z-Wave

* Bezdrátový komunikační protokol s nízkou spotřebou energie pro sítě domácí automatizace (HAN).

* Protokol pokrývá peer-to-peer komunikaci na vzdálenost přibližně 30 metrů a je určen pro aplikace, které vyžadují přenos drobných dat, jako je ovládání osvětlení, ovládání domácích spotřebičů, inteligentní energetika a HVAC, kontrola přístupu, ovládání nositelné zdravotní péče a detekce požáru. 

* Systém Z-Wave pracuje v pásmech ISM (kolem 900 MHz) a umožňuje přenosovou rychlost 40 kb/s.

---

# Z-Wave

* Nejnovější verze podporují také rychlost až 200 kb/s. 
  * Její vrstva MAC využívá mechanismus pro zamezení kolizí
  * Spolehlivý přenos je v tomto protokolu možný pomocí volitelných zpráv ACK.

* V jeho architektuře existují řídicí a podřízené uzly. 
  * Řadiče řídí podřízené uzly vysíláním příkazů na příkazů. 
  * Pro účely směrování vede řadič tabulku topologie celé sítě. 
  * Směrování v tomto protokolu je prováděno metodou zdrojového směrování, při níž řadič předkládá cestu uvnitř paketu.

---

# Z-Wave vs. Zigbee - co mají společného?

* Obě technologie jsou sítě mesh
  * Každý uzel v systému funguje jako zdroj bezdrátových dat i jako opakovač. Informace z jednoho senzorového uzlu přeskakují z uzlu do uzlu, dokud přenos nedorazí k bráně.

* Obě technologie využívají protokol IEEE 802.15.4 pro osobní sítě s nízkou rychlostí (LR-PAN).
  * Pro sjednocenou fyzickou vrstvu (1. vrstva OSI), strukturování paketů a vytváření schémat MAC (Medium Access Control).

* Obě jsou široce používány v lokálních datových sítích se senzory
  * například v bezpečnostních systémech, řídicích jednotkách městských inteligentních sítí, řízení HVAC, domácí automatizaci a řízení osvětlení

---

# Z-Wave vs. Zigbee - čím se liší?

* Z-wave má přísně kontrolovaný ekosystém produktů, který je určen pro inteligentní domácnosti a inteligentní budovy, zatímco Zigbee lze použít pro řadu aplikací.

* Nepředpokládá se, že by dvě zařízení Zigbee byla interoperabilní, pokud není interoperabilita předem naplánována. 

* Naproti tomu aplikace Z-Wave se téměř vždy integruje s jiným zařízením Z-Wave.

---

# Z-Wave vs. Zigbee - čím se liší?

* Zigbee používá globální standardní frekvenční pásmo ISM 2,4 GHz, zatímco Z-Wave používá pásmo ISM 915 MHz (v USA) a pásmo RFID 868 MHz (v Evropě).

* Pásmo -2,4 GHz může být vystaveno intenzivnímu rušení ze strany systémů WiFi a Bluetooth, zatímco subgHz pásma, která používá Z-Wave, se se stejnými problémy s rušením nepotýkají.

* Mnoho poskytovatelů vyrábí rádia Zigbee, ale Z-Wave používá proprietární rádiový systém od společnosti Sigma designs.

* Z-Wave používá modulaci FSK, zatímco modulace Zigbee probíhá prostřednictvím přímo sekvenčně rozprostřeného spektra (DSSS).

---

# LTE-A (Long-Term Evolution Advanced)

* LTE-A je škálovatelný protokol s nižšími náklady ve srovnání s jinými buněčnými protokoly.

* LTE-A využívá technologii OFDMA (Orthogonal Frequency Division Multiple Access) jako přístupovou technologii na úrovni MAC, která rozděluje frekvenci do více pásem a každé z nich lze používat samostatně.

* Architektura LTE-A se skládá z páteřní sítě (CN), rádiové přístupové sítě (RAN) a mobilních uzlů.

  * CN je zodpovědná za řízení mobilních zařízení a sledování jejich IP adres.

  * RAN odpovídá za vytvoření řídicí a datové roviny a za řízení bezdrátového připojení a rádiového přístupu.

---

# Nelicencovaná RF pásma

* **433 MHz** (Evropa)
* **868 MHz** (Evropa)
* **915 MHz** (Amerika, Asie)

<hr>

* V těchto kanálech dochází k velkému rušení, protože jsou používány různé druhy modulací (AM, FM) a různé modely kódování.
* Užitečné pro krátký dosah až střední dosah na velmi jednoduchých zařízeních s nízkým příkonem
* Pouze peer-to-peer komunikace
* Žádná infrastruktura
* Žádné normy (specifikace, jako např. LoRaWAN, není norma)

---

# Domácí a průmyslová automatizace

---

# X10
	
* [X10](https://en.wikipedia.org/wiki/X10_(industry_standard)) je protokol pro komunikaci mezi elektronickými zařízeními používanými pro domácí automatizaci. 

* K signalizaci a ovládání využívá především vedení elektrického proudu.

* Řídicí jednotky X10 vysílají signály po stávajícím vedení střídavého proudu do přijímacích modulů.

* Technologie X10 přenáší binární data pomocí techniky amplitudové modulace (AM).

* Data jsou zakódována na nosné frekvenci 120 kHz, která je přenášena v sériích během vysílání. relativně tichých přechodů nulou střídavého proudu o frekvenci 50 nebo 60 Hz. proudu. Při každém přechodu nuly se přenáší jeden bit.

---

# HomePlug (IEEE 1901)

* [IEEE 1901](https://en.wikipedia.org/wiki/IEEE_1901) je standard pro vysokorychlostní (až 500 Mbit/s na fyzické vrstvě) komunikační zařízení po elektrických vedeních, často nazývaný širokopásmové připojení po elektrických vedeních (BPL). 

* Norma používá přenosové frekvence nižší než 100 MHz.

  * HomePlug 1.0
  * HomePlug AV
  * HomePlug AV2
  * HomePlug Green PHY (Energy efficient)

* Convergence Digital Home (IEEE 1905.1)
	* Umožňuje spolupráci protokolů domácí automatizace pro bezdrátovou komunikaci a komunikaci po elektrickém vedení.

---

# BACnet (ISO 16484-5)

* [BACnet](https://en.wikipedia.org/wiki/BACnet) - Building Automation and Control networks.

* BACnet byl navržen tak, aby umožňoval komunikaci systémů automatizace a řízení budov pro aplikace, jako jsou např. řízení vytápění, větrání a klimatizace, řízení osvětlení, řízení přístupu, systémy detekce požáru a další. jejich přidružená zařízení.

* Protokol BACnet definuje řadu služeb, které se používají ke komunikaci mezi zařízeními v budovách. Mezi služby protokolu patří Who-Is, I-Am, Who-Has, I-Have, které se používají pro zjišťování zařízení a objektů. Služby Read-Property a Write-Property se používají pro sdílení dat. Podle normy ANSI/ASHRAE 135-2016 definuje protokol BACnet 60 typů objektů, s nimiž služby pracují. 

* Funguje v sítích [ARCNET](https://cs.wikipedia.org/wiki/ARCNET), Ethernet, RS-232, RS-485, ZigBee či [LonTalk](https://cs.wikipedia.org/wiki/LonTalk).

---

# ModBus, FieldBus, IE

* Otevřený datový komunikační protokol široce používaný pro připojení průmyslových elektronických zařízení.
  * otevřená struktura, flexibilní, široce známý
* Dodáván mnoha softwary SCADA (Supervisory Control And Data Acquisition)
* 2 režimy sériového přenosu:
  * ASCII, RTU (binární)
* Komunikační rozhraní
  * RS-232/485, Ethernet (TCP/IP) 
* Další protokoly pro průmyslovou automatizaci:
  * FieldBus (IEC 61158).
  * Průmyslový Ethernet (robustní Ethernet)

---

# RFID - ISO/IEC 18000

* ISO/IEC 18000 je mezinárodní norma, která popisuje řadu různých technologií RFID, z nichž každá využívá	vlastní frekvenční rozsah

  * 18000-1: obecné vlastnosti a popis technologie
  * 18000-2: méně než 135 kHz
  * 18000-3: HF 13,56 MHz
  * 18000-4: 2,42 GHz
  * 18000-6: UHF 860-960 MHz
  * 18000-7: 433 MHz

* NFC je odnož vysokofrekvenční (HF) RFID, která pracuje na frekvenci 13,56 MHz. NFC je navrženo jako bezpečná forma výměny dat a zařízení NFC může být jak čtečkou NFC, tak značkou NFC. Tato jedinečná vlastnost umožňuje zařízením NFC komunikovat mezi sebou.

---

# Síťová vrstva pro IoT

---

# Síťová vrstva - IPv4/IPv6

* IP (1974) $2^8$ zařízení v $2^4$ sítích (RFC 675)

* IPv4 (1981) $2^{32}$

* Není to málo, Antone Pavloviči? No je...

* Adresní prostor lze logicky pokrýt IPv6, ale komplexita implementace je příliš vysoká

---

# Jak z toho ven?

* IPv4/IPv6 předpokládají hvězdicový model spojení, kde centrálním bodem je přepínač/směrovač.
  * Jedná se o spojení M2M (ale singulární, nevyplývá z předpokladů IoT).
  * Nepodporuje mesh

* Zařízení musí zvládnout směrování k blízkým účastníkům v rámci sítě a lokálně udržovat svůj seznam (ARP)
  * Obrovské (!) paměťové nároky

* Musí existovat správce pro správu IP adres (obvykle DHCP server).
  * Zařízení musí kontaktovat tohoto správce, aby získala adresu.

**Řešením je 6LoWPan, navržené přímo pro IoT.**  

---

# Síťová vrstva pro IoT - 6LoWPAN

* Vrstvy PHY a MAC jsou definovány v normě 802.15.4.
  * Má 3 režimy fyzické vrstvy: 20, 40 a 250 kb/s.
	* Je určena pro RF komunikaci v pásmech 800, 900 MHz a 2,4 GHz

* Podporuje 64bitové i 16bitové režimy adresování.

* Podporuje proměnlivou velikost rámce, pokud jde o užitečné zatížení.
  * Základní velikost rámce je 127 bajtů.
  * Porovnejte s IPv6, kde je velikost rámce 1280 bajtů.

---

# 6LoWPAN - směrování, transport

* Podporuje unicast a broadcast.

* Podporuje směrování IP a síťování na linkové vrstvě (802.15.5)!
  * Pozor, síť/mesh na linkové vrstvě může mít také mnoho bran do internetu (ne nutně jen jednu, jak je často prezentováno)

* Pro zjišťování sousedních zařízení používá automatickou konfiguraci sítě.

* Podporuje zabezpečení
  * AES 128 bitů (také 64) - obvykle se provádí prostřednictvím specializovaného hardwarového řešení (čipu)
				
* Primární transportní vrstva je UDP.

* Podporuje až 64000 uzlů v rámci topologie (uzly a směrovače) s 16bitovou adresou

---

# Architektura 6LoWPAN

![h:550px center](figs/iot-12.png)

---

# Jak 6LoWPAN funguje?
	
* Edge router překládá adresy mezi 6LoWPAN a IPv6 (případně IPv4).

* Adresy 6LoWPAN jsou "komprimované" adresy IPv6
  * Funguje na principu plochého adresního prostoru (jedna podsíť).
  * S jedinečnými adresami MAC (64 nebo 16 bitů dlouhými)

* Spuštění sítě 6LoWPAN (while (true) repeat 1-3:)
  1. Commissioning - Založení spojení (linková vrstva) mezi uzly.
	2. Bootstrapping - Konfigurace adres, discovery a registrace.
  3. Route init - Provedení směrovacího algoritmu pro nastavení cest.
 
---

# 6LoWPAN discovery

* Běžné discovery sítě IPv6 zde nefunguje, protože předpokládá, že zařízení jsou vždy online (což v IoT není pravda).

* Zjišťování sítě (sousedních zařízení) v síti 6LoWPAN využívá 3 principy (RFC6775):

  * **NR** - registrace uzlů (Network Registration)
  * **NC** - potvrzení uzlu (Network Confirmation)
  * **DAD** -- detekce duplicitních adres (Duplicate Address Discovery)

* [+] Podpora infrastruktury okrajových směrovačů

---

# 6LoWPAN - Neighbor Discovery

* Nový uzel vysílá RS (Router Solicitud) multicast	
* Všechny routery odpovídají RA (Router Advertisement) unicast	
* Uzel vybírá jeden router (zpravidla první RA) a sestavuje globální IPv6 adresu na základě prefixu
* Uzel posílá ARO (Adress Registration Option) unicast zvolenému routeru
* Router odpovídá ARO a status
  * Status: OK, duplicate address, cache full
  * Pokud OK, router vloží adresu uzlu do cache
* Uzel periodicky informuje router o tom, že žije (NUD, Neighbor Unreachability Detection)

---

# 6LoWPAN - řešení duplicitních adres

* Během procesu registrace požádá router všechny edge routery o ověření, zda požadovaná adresa je unikátní

* DAD probudí IoT zařízení ze standby režimu

---

# 6LoWPAN - Network registration

* Uzel odešle routeru unicastovou výzvu DODAG (Destination Oriented Directed Acyclic Graph).

* Router odpoví informačním objektem DODAG (DIO) a pravidelně jej vysílá. DIO obsahuje pořadí routeru (tj. uvádí, jak daleko je router od edge routeru).

* Pokud uzel získá DIO s lepší hodností, měl by znovu zaregistrovat u jiného "lepšího" routeru jako nový výchozí směrovače.

* Nakonec uzel odešle Destination Advertising Object (DAO) svému výchozímu routeru, který je následně předán edge routeru.

* Edge router odpoví DAO ACK.

---

![h:600px center](figs/iot-13.png)

---

# 6LoWPAN - shrnutí

* Síťová vrstva schopná vytvářet samoorganizující mesh sítě (navíc škálovatelné)

* Určena i pro sítě typu hvězda (v tom případě je edge router shodný s routerem)

* Používá malé datové rámce a zjednodušený model adresování

* Schopná detekce duplicitních adres









