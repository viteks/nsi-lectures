---
marp: true
paginate: true

style: |
    img[alt~="right"] {
      position: relative;
      top: -120px;
      float: right;
    }
    img[alt~="center"] {
      display: block;
      margin: 0 auto;
      padding-top: 20px; 
    }
    section{
      
    }
    strong {color: #455a64; font-weight: 600;}

    div.twocols {
        margin-top: 35px;
        column-count: 2;
    }
    div.twocols p:first-child,
    div.twocols h1:first-child,
    div.twocols h2:first-child,
    div.twocols ul:first-child,
    div.twocols ul li:first-child,
    div.twocols ul li p:first-child {
        margin-top: 0 !important;
    }
    div.twocols p.break {
        break-before: column;
        margin-top: 0;
    }
---

<!-- _paginate: false -->

# Návrh systémů IoT

## 10. CI/CD

Stanislav Vítek
Katedra radioelektroniky
České vysoké učení technické v Praze

<!--
---

# Obsah přednášky

-->
---

# Co je CI/CD pipeline?

* CI/CD pipeline automatizuje proces dodávání softwaru. 
  * Sestavuje kód, spouští testy a pomáhá bezpečně nasadit novou verzi softwaru. 
  * Snižuje počet manuálních chyb, poskytuje zpětnou vazbu vývojářům a umožňuje rychlé iterace produktu.

* CI/CD zavádí automatizaci a nepřetržité sledování v průběhu celého životního cyklu softwarového produktu. 
  * Zahrnuje fázi od integrace a testování až po dodání a nasazení.

---

# Continuous integration, delivery, deployment

* **Kontinuální integrace** je metoda vývoje softwaru, při které mohou členové týmu integrovat svou práci alespoň jednou denně. Při této metodě je každá integrace kontrolována automatickým sestavením, které hledá chyby.

* **Kontinuální dodávka** je metoda softwarového inženýrství, při níž tým vyvíjí softwarové produkty v krátkém cyklu. Zajišťuje, že software může být kdykoli snadno uvolněn.

* **Kontinuální nasazení** je metoda softwarového inženýrství, při níž jsou funkce produktu dodávány pomocí automatického nasazení. Pomáhá testerům ověřit, zda jsou změny v kódové základně správné a zda je stabilní, či nikoli.

---

# Fáze CI/CD pipeline

![h:550px center](figs/DevOps-Stages-CI-CD-Pipeline-Edureka.png)

---

# Zdrojová etapa

* Ve zdrojové fázi je CI/CD pipeline spouštěna úložištěm kódu. 
* Jakákoli změna v programu spustí oznámení nástroji CI/CD, který spustí ekvivalentní pipeline. 
* Mezi další běžné spouštěče patří pracovní postupy iniciované uživatelem, automatizované plány a výsledky jiných pipeline.

---

# Fáze sestavování

* Jedná se o druhou fázi CI/CD Pipeline, ve které sloučíte zdrojový kód a jeho závislosti. Provádí se hlavně proto, abyste vytvořili spustitelnou instanci softwaru, kterou můžete potenciálně odeslat koncovému uživateli.

* Programy napsané v jazycích, jako je C++, Java, C nebo jazyk Go, by měly být zkompilovány. Naproti tomu programy v jazycích JavaScript, Python a Ruby mohou pracovat bez fáze sestavení.

* Neúspěch ve fázi sestavení znamená, že došlo k zásadní chybě v konfiguraci projektu, proto je lepší takový problém okamžitě řešit.

---

# Testovací fáze

* Fáze testování zahrnuje provádění automatizovaných testů, které ověřují správnost kódu a chování softwaru. Tato fáze zabraňuje tomu, aby se snadno reprodukovatelné chyby dostaly ke klientům. Za psaní automatizovaných testů jsou zodpovědní vývojáři.

# Fáze nasazení

* Jedná se o poslední fázi, ve které se váš produkt uvádí do provozu. Jakmile sestavení úspěšně projde všemi požadovanými testovacími scénáři, je připraveno k nasazení na ostrý server.

---

# Údržba zdrojového kódu

---

# Git

* Uvolněn 7. dubna 2005, autorem je Linus Torvalds
* Původně určen jako náhrada systému Bitkeeper
* Proč Git používat?
  * Správa různých verzí projektu
  * Historie
  * Primárně je lokální (= nepotřebuje přístup na Internet)

<br>

**Literatura:** Scott Charcon, Pro Git, [PDF](https://knihy.nic.cz/files/nic/edice/scott_chacon_pro_git.pdf)  

---

# Principy Gitu

* Každý soubor je uložen jen jednou, další verze uloženy jen jako **snapshoty**
* Soubory jsou ukládány binárně (takže lze verzovat nrpř. i obrázky)
* Každá operace je nejprve lokální
* Pro každý soubor nebo složku Git spočítá kontrolní součet
* Git pouze přidává data. I když vymažeme řádek, Git pouze dostane informaci o přidání *ničeho* do souboru.

--- 

# Pracovní prostory

## Working directory

* Working directory je naše složka, ve které pracujeme. 
* Nikde ovšem není přikázáno, že všechny soubory v naší složce musí být zároveň v repositáři. 
* Git nás nenutí k určitému postupu práce. Jak budeme pracovat je čistě na nás, Git se přizpůsobí.

![bg h:500px right](figs/git-areas.png)

---

# Pracovní prostory

## Staging area

* Pro soubory, které chceme, aby Git zpracoval, slouží staging
area. 
* Je to taková virtuální složka pro Git. 
* Vše, co se do ní dostane, bude v následujícím commitu přeneseno do lokálního repositáře.

![bg h:500px right](figs/git-areas.png)

---

# Pracovní prostory

## Lokální repositář 

* Lokální repositář je naše historie projektu, na kterém pracujeme. 
* V něm jsou uloženy všechny snapshoty a všechny předchozí verze systému. 
* Vše co se dostane do repositáře už prakticky není možné smazat.

![bg h:500px right](figs/git-areas.png)

---

# Pracovní prostory

## Vzdáledný repozitář 

* Na vzdálený repositář nahráváme lokální repositář. 
* Jde obnovit pracovní složku do stavu, v jakém byl aktuální stav vzdáleného repositáře. 
* Jde také obnovit složku do stavu při jednom z commitů 

![bg h:500px right](figs/git-areas.png)

---

# Vytvoření repozitáře


* Nový repozitář, lze vytvořit i v adresáři s rozpracovaným projektem
* Repozitář se vytvoří až prvním commitem

```
git init
```

* Stažení existujícího repozitáře

```
git clone https://github.com/git/git.git 
```

**Commit**

* Obsahuje snapshot všech souborů, které chceme sledovat. 
* Kromě toho commit obsahuje informaci o tom, kdo a kdy jej vytvořil.

---

# Stav repozitáře

```
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean
```

Získané informace

* Jsme na větvi master. Git umožňuje pracovat na více věcích současně pomocí tzv. větví mezi kterými můžeme přepínat. Pak se na na disku „objeví” verze souborů v dané větvi. 
* Žaše větev master je aktuální vůči větvi na vzdáleném repozitáři (origin).
* Že jsme neudělali žádné změny oproti poslední verzi souborů, což je pravda.

---

# První revize - commit

* Přidáme do projektu nový soubor *soubor.txt*
* Zkontrolujeme stav repozitáře
```
git status
```
* Pridáme nový soubor do stage
```
git add soubor.txt
```
* Přidáme revizi
```
git commit
```

---

![](figs/diagram.png)

---

# Sestavování - příklad Flask aplikace

* Příprava prostředí pro běh Python aplikace

```
git clone git@gitlab.fel.cvut.cz:iot-lab/nsi-test.git
cd nsi-test
python -m venv env
env\Scripts\activate.bat
pip install flask
pip freeze > requirements.txt
```

* Do prostředí přidáme jednoduchou Flask aplikaci

```
git add app\main.py requirements.txt
git commit -a
git push origin main
```
---



---

# Testování

* Typů testů je hned několik
* Obvykle nepokrývají úplně všechny možné scénáře (všechen kód)
* Hovoříme o procentuálním pokrytí testy (code coverage), většinou kritických částí aplikace. 
* Čím větší aplikace je, tím více typů testů potřebuje a tím více funkčnosti obvykle pokrýváme. 
* První verze menších aplikací většinou naopak ještě nepotřebují žádné testy nebo jen ty úplně základní, např. aby se do nich dalo registrovat.

---

# Jednotkové testy (Unit testy) 

* Testují univerzální knihovny, nepíšeme je pro kód specifický pro danou aplikaci. 
* Jednotkové testy testují třídy, přesněji jejich metody, jednu po druhé. Předávají jim různé vstupy a zkouší, zda jsou jejich výstupy korektní. 
  * Nemá úplně smysl pomocí unit-testů testovat, zda metoda obsahující databázový dotaz, která je použita v jednom kontroleru (nebo nějaké jiné řídící vrstvě) vrací co má. 
  * Naopak dává velmi dobrý smysl testovat např. validátor telefonních čísel, který používáme na 20 místech nebo dokonce v několika aplikacích. 
* Unit testy jsou tzv. **whitebox testy**, to znamená, že je píšeme s tím, že víme, jak testovaný kód uvnitř funguje (vidíme dovnitř).

---

# Akceptační testy

* Tento typ testů je naopak úplně odstíněn od toho, jak je aplikace uvnitř naprogramovaná, jsou to tedy **blackbox testy** (nevidíme dovnitř). 
* Každý test obvykle testuje určitou funkčnost, např. test pro psaní článků by testoval jednotlivé **use cases**
  * předat článek ke schválení,
  * schválit článek,
  * zamítnout článek,
  * publikovat článek jako administrátor ... 
* Těmito testy se tedy v podstatě zkouší specifická logika aplikace (databázové dotazy a podobně), testuje se výsledek, který aplikace vygeneruje, ne přímo její vnitřní kód.

---

# Integrační testy

* V dnešní době dosahují aplikace již poměrně vysoké komplexnosti
* Velmi často bývají rozdělené do několika služeb, které spolu komunikují a jsou vyvíjené zvlášť. 

* Právě integrační testy dohlíží na to, aby do sebe vše správně zapadalo.

---

# Systémové testy

* I když aplikace funguje dobře, na produkčním prostředí podléhá dalším vlivům

* S vnějšími vlivy je třeba počítat a připravit se na ně

  * aplikace zvládá obsluhovat tisíc uživatelů v jeden okamžik,
  * výpadek internetového připojení,
  * selhání microservice

* To bychom provedli zátěžovým testem, který spadá mezi systémové testy.

---

# V-model vývoje aplikace

![h:500px center](figs/testovani_v_model.png)

---

# Příklad testování

Než začneme:

* Testy tedy píšeme vždy na základě návrhu, nikoli implementace
* Děláme je na základě očekávané funkčnosti
  * Požadavek od zákazníka (akceptační testy)
  * Architekt aplikace
* Nikdy nepíšeme testy podle toho, jak je něco uvnitř naprogramované!
* Testování s implementací ve skutečnosti vůbec nesouvisí, vždy testujeme, zda je splněno zadání.

---

# Příklad projektu

```python
class Kalkulacka:
    # reprezentuje jednoduchou kalkulačku
    # proměnná a v níže uvedených funkcích je první číslo, proměnná b číslo druhé

    def secti(a, b):    # sečte 2 čísla a vrátí jejich součet
        return a + b

    def odecti(a, b):   # odečte 2 čísla a vrátí jejich rozdíl
        return a - b

    def vynasob(a, b):  # vynásobí 2 čísla a vrátí jejich součin
        return a * b

    def vydel(a, b):    # vydělí 2 čísla a vrátí jejich podíl
        if b == 0:
            raise ValueError('Nelze dělit nulou!')
        return a / b
```
---

# Generování testů

* Často používaným grameworkem pro unit testování v Pythonu je **unittest**

* Testy většinou nepíšeme do stejného souboru, ale pro přehlednost vložíme kód do nového souboru

  * Kromě modulu unittest a importujeme třídu, kterou budeme testovat
  * Poté si vytvoříme testovací třídu, která je odvozena od třídy TestCase testovacího modulu unittest a zdědí z ní řadu užitečných metod

* Do třídy je zvykem přidat classmethod s názvem setUpClass(cls), která se zavolá jednou na začátku před všemi testy. 
* Přidáme další metodu s názvem tearDownClass(cls), která se zavolá jednou na konci, poté co testy proběhnou.

---

```python
# test_kalkulacka.py
import unittest
from kalkulacka import Kalkulacka

# vytvoření testovací třídy, která dědí ze třídy TestCase
class TestKalkulacka(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass # volá se před začátkem všech testů

    @classmethod
    def tearDownClass(cls):
        pass # volá se po ukončení všech testů

    def setUp(self):
        pass # volá se před začátkem každého testu

    def tearDown(self): # volá se po ukončení každého testu, vytvoří novou kalkulačku
        from kalkulacka import Kalkulacka
        Kalkulacka = Kalkulacka()
```
---

# Testovací metody

Pro testování se používají **assert** metody z třídy **unittest.TestCase**
* assertEqual() - porovná aktuální hodnotu s hodnotou očekávanou
* assertAlmostEqual() - přidává toleranci

```python
def test_vydel(self):
    # testuje funkci vydel ze souboru kalkulacka.py
    self.assertEqual(Kalkulacka.vydel(4, 2), 2)
    self.assertAlmostEqual(Kalkulacka.vydel(3.14, -1.72), -1.826, 3)
    self.assertEqual(Kalkulacka.vydel(1.0/3, 1.0/3), 1)

def test_vydel_nulou(self):
    # testuje funkci vydel_nulou ze souboru kalkulacka.py
    with self.assertRaises(ValueError):
        Kalkulacka.vydel(2, 0)
```
---

# Další assert metody

* assertTrue(x) - zkontroluje, zda booleanovská hodnota (x) vyjde jako True
* assertFalse(x) - zkontroluje, zda booleanovská hodnota (x) vyjde jako False
* assertIs(a, b) - zkontroluje, zda první a druhá hodnota jsou stejný objekt
* assertIsNot(a, b) - zkontroluje, zda první a druhá hodnota nejsou stejný objekt
* assertIsNone(x) - zkontroluje, zda se hodnota x rovná None
* assertIsNotNone(x) - zkontroluje, zda se hodnota **x** nerovná **None**
* assertIn(a, b) - zkontroluje, zda se hodnota a nachází v kontejneru b
* assertNotIn(a, b) - zkontroluje, zda se hodnota a nenachází v kontejneru b
* assertIsInstance(a, b) - zkontroluje, zda je objekt a instancí třídy b
* assertNotIsInstance(a, b) - zkontroluje, zda objekt a není instancí třídy b

---

# Spuštění testů

* Z příkazové řádky
* Testují se soubory s předponou **test_**, jiné soubory explicitně zadáme za parametrem **-v**

```
python -m unittest

test_odecti (test_kalkulacka.TestKalkulacka) ... ok
test_secti (test_kalkulacka.TestKalkulacka) ... ok
test_vydel (test_kalkulacka.TestKalkulacka) ... ok
test_vydel_nulou (test_kalkulacka.TestKalkulacka) ... ok
test_vynasob (test_kalkulacka.TestKalkulacka) ... ok

----------------------------------------------------------------------
Ran 5 tests in 0.004s

OK
```
---

# Co MicroPython?

* Nástroj [pyboard.py](https://docs.micropython.org/en/latest/reference/pyboard.py.html#the-pyboard-py-tool) umožňuje spouštět příkazy přímo na zařízení
* Není tak nutné používat IDE nebo sériovou konzoli

```
python pyboard.py --device $TARGET_PORT --command "import sys; print(sys.implementation)"
```

* Je možné spouštět Python program bez toho, aby byl na zařízení nahraný

```
python pyboard.py --device /dev/ttyACM0 app.py
```

* Je možné i kopírovat programy na zařízení

```
python pyboard.py --device /dev/ttyACM0 -f cp main.py :
```

