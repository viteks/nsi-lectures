---
marp: true
paginate: true

style: |
    img[alt~="right"] {
      position: relative;
      top: -120px;
      float: right;
    }
    img[alt~="center"] {
      display: block;
      margin: 0 auto;
      padding-top: 20px; 
    }
    section{
      
    }
    strong {color: #455a64; font-weight: 600;}

    div.twocols {
        margin-top: 35px;
        column-count: 2;
    }
    div.twocols p:first-child,
    div.twocols h1:first-child,
    div.twocols h2:first-child,
    div.twocols ul:first-child,
    div.twocols ul li:first-child,
    div.twocols ul li p:first-child {
        margin-top: 0 !important;
    }
    div.twocols p.break {
        break-before: column;
        margin-top: 0;
    }
---

<!-- _paginate: false -->

# Návrh systémů IoT

## 6. Sběrnice a přenos dat. Programování MCU, RTOS

Stanislav Vítek
Katedra radioelektroniky
České vysoké učení technické v Praze

---

# Obsah přednášky

### Přístrojové sběrnice

* [SPI](#3), [I2C](#6)

### Serializace
* [ujson, pickle](#17), [MessagePack](#21), [Protocol Buffers](#22)

### V čem se dá ještě programovat (nejen RPi Pico)? 

### RTOS

---

# SPI

* Sériová sběrnice, full duplex
* Single master, multiple slave
* MISO, MOSI, SCLK, SS/CS
* Typická rychlost 10Mbps
* Vzdálenost do 10m
* Jednoduchá implementace
  * není třeba adresace
* Nevýhody
  * více zařízení = více drátů
  * bez kontroly dat

![h:400px bg right](figs/SPI-protocol-wiring-one-master-multiple-slave.jpg)

---

#  SPI v MicroPythonu

* Sběrnice SPI je popsána třídou [machine.SPI](https://docs.micropython.org/en/latest/library/machine.SPI.html), SW i HW implementace

```python
from machine import SPI, Pin

spi = SPI(0, baudrate=400000)       # Create SPI peripheral 0 at frequency of 400kHz.
                                    # Depending on the use case, extra parameters may be required
                                    # to select the bus characteristics and/or pins to use.
cs = Pin(4, mode=Pin.OUT, value=1)  # Create chip-select on pin 4.

try:
  cs(0)                             # Select peripheral.
  spi.write(b"12345678")            # Write 8 bytes, and don't care about received data.
finally:
   cs(1)                            # Deselect peripheral.
```

---

# SPI - konstruktory a metody

* Hardware [SPI](https://docs.micropython.org/en/latest/library/machine.SPI.html#machine.SPI)
```python
class machine.SPI(id, ...)
```

* Software [SoftSPI](https://docs.micropython.org/en/latest/library/machine.SPI.html#machine.SoftSPI)
```python
class machine.SoftSPI(baudrate=500000, *, polarity=0, phase=0, bits=8, 
                      firstbit=MSB, sck=None, mosi=None, miso=None)
```

* inicializace [init()](https://docs.micropython.org/en/latest/library/machine.SPI.html#machine.SPI.init), [deinit()](https://docs.micropython.org/en/latest/library/machine.SPI.html#machine.SPI.deinit)

* zápis [read()](https://docs.micropython.org/en/latest/library/machine.SPI.html#machine.SPI.read), [readinto()](https://docs.micropython.org/en/latest/library/machine.SPI.html#machine.SPI.readinto)
* čtení [write()](https://docs.micropython.org/en/latest/library/machine.SPI.html#machine.SPI.write), [write_readinto()](https://docs.micropython.org/en/latest/library/machine.SPI.html#machine.SPI.write_readinto)

---

# I2C

* Sériová synchronní sběrnice, half duplex
  * multimaster, multislave, dva vodiče: SDA, SCL
  * podporuje rychlosti 100 kbps, 400 kbps, a 3.4 Mbps (příp. 10 kbps a 1 Mbps)
  * spíše kratší vzdálenosti, typicky max. 30 cm

![h:200px center](figs/i2c-protocol-wiring-multiple-master-multiple-slave.jpg)

---

# I2C protokol

![h:100px center](figs/protocol-i2c-data-packet.jpg)

* START: SDA se přepne HIGH->LOW před tím, než SCL přepne HIGH->LOW
* Stop: SDA se přepne LOW->HIGH poté, co SCL přejde LOW->HIGH
* Adresní rámec: 7 nebo 10bitová adresa slave, se kterým chce master komunikovat.
* Bit pro čtení/zápis: bit určující, zda master posílá data podřízenému zařízení (LOW) nebo od něj požaduje data (HIGH).
* ACK/NACK Bit: Za každým rámcem ve zprávě následuje potvrzovací/nepotvrzovací bit. Pokud byl rámec adresy nebo dat úspěšně přijat, je odesílateli vrácen bit ACK od přijímajícího zařízení.

---

# I2C v MicroPythonu

* Sběrnice I2C je popsána třídou [machine.I2C](https://docs.micropython.org/en/latest/library/machine.I2C.html)

```python
from machine import I2C

i2c = I2C(freq=400000)          # create I2C peripheral at frequency of 400kHz
                                # depending on the port, extra parameters may be required
                                # to select the peripheral and/or pins to use

i2c.scan()                      # scan for peripherals, returning a list of 7-bit addresses

i2c.writeto(42, b'123')         # write 3 bytes to peripheral with 7-bit address 42
i2c.readfrom(42, 4)             # read 4 bytes from peripheral with 7-bit address 42

i2c.readfrom_mem(42, 8, 3)      # read 3 bytes from memory of peripheral 42,
                                #   starting at memory-address 8 in the peripheral
i2c.writeto_mem(42, 2, b'\x10') # write 1 byte to memory of peripheral 42
                                #   starting at address 2 in the peripheral
```

---

# I2C - konstruktory a metody

* Hardware [I2C](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C)
```python
class machine.I2C(id, *, scl, sda, freq=400000)
```
* Software [SoftI2C](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.SoftI2C)
```python
class machine.SoftI2C(scl, sda, *, freq=400000, timeout=50000)
```

* Inicializace [init()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.init) a deinicializace [deinit()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.deinit)
* Skenování [scan()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.scan)
  * prohledá všechny adresy I2C mezi 0x08 a 0x77 včetně a vrátí seznam těch, které odpovídají
  * zařízení reaguje, pokud po odeslání své adresy (včetně zapisovacího bitu) na sběrnici přitáhne SDA.

---

# I2C - nízkoúrovňové funkce třídy SoftI2C

* [start()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.start) - START podmínka (SDA přechází na LOW hodnotu, zatímco SCL je HIGH).

* [stop()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.stop) - STOP podmnínka (SDA přechází na HIGH, zatímco SCL je HIGH)

* [readinto()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.readinto) - čte data ze sběrnice a ukládá je do bufferu. 
  * Počet načtených bajtů odpovídá délce bufferu.
  * Po přijetí všech bajtů kromě posledního se na sběrnici odešle ACK. Po přijetí posledního bajtu lze odesláním NACK nebo ACK rozli3it, zda se budou číst data v pozdějším volání.

* [write()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.write) - zápis dat z bufferu na sběrnici. 
  * Kontroluje, zda je po každém bajtu přijato ACK, a v případě přijetí NACK přeruší zápis zbývajících dat. Funkce vrací počet přijatých ACK.

---

# Standardní I2C operace - čtení

```python
I2C.readfrom(addr, nbytes, stop=True, /)
```

* [readfrom()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.readfrom) - přečte nbytes z periferie zadané pomocí addr. 
  * Pokud je stop true, pak je na konci přenosu generována podmínka STOP. 
  * Vrací objekt bytes s přečtenými daty.

```python
I2C.readfrom_into(addr, buf, stop=True, /)
```

* [readfrom_into()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.readfrom_into) - čtení do buf z periferie zadané pomocí addr. 
  * Počet přečtených bajtů bude odpovídat délce buf. 
  * Pokud je stop true, pak je na konci přenosu generována podmínka STOP.

---

# Standardní I2C operace - zápis

```python
I2C.writeto(addr, buf, stop=True, /)
```

* [writeto()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.writeto) - zapíše bajty z buf do periferie zadané pomocí addr. Pokud je po zápisu bajtu z buf přijat NACK, zbývající bajty se neodešlou. Pokud je stop true, pak je na konci přenosu generována podmínka STOP, i když je přijat NACK. Funkce vrací počet přijatých ACK.

```python
I2C.writevto(addr, vector, stop=True, /)
```

* [writevto()](https://docs.micropython.org/en/latest/library/machine.I2C.html#machine.I2C.writevto) - zapíše bajty obsažené ve vektoru na periferii zadanou pomocí addr. vektor by měl být tuple nebo seznam objektů s protokolem bufferu. Addr se odešle jednou a pak se postupně vypíší bajty z každého objektu ve vektoru. Objekty ve vektoru mohou mít délku nula bajtů, v takovém případě se na výstupu nepodílejí.

---

# Serializace dat

---

# Serializace dat

* Potřeba serializace vzniká vždy, když je třeba data přenášet přes rozhraní, jako je socket, UART nebo rozhraní jako I2C nebo SPI. 
* Přenos dat sériovým rozhraním vyžaduje, aby data byla prezentována jako lineární posloupnosti bajtů. 
* **Problém:** jak převést libovolný objekt (třeba v Pythonu) na takovou posloupnost a jak následně objekt obnovit.

* Pro dosažení tohoto cíle existuje řada standardů, z nichž pět je pro MicroPython snadno dostupných. Každý z nich má své výhody a nevýhody. Ve dvou případech je cílem zakódovaných řetězců, aby byly čitelné pro člověka a obsahovaly znaky ASCII. V ostatních se skládají z binárních bajtových objektů, kde bajty mohou nabývat všech možných hodnot.

---

#  Serializace dat v MicroPythonu

1. ujson (ASCII, official)

2. pickle (ASCII, official)

3. ustruct (binary, official)
    * vyžaduje, aby vysílací a přijímací strana sdíleli schéma, délka zprávy je fixní

4. MessagePack binary, [unofficial](https://github.com/peterhinch/micropython-msgpack)
    * umožňuje, aby se za běhu měnila struktura i délka zprávy

5. protobuf binary, [unofficial](https://github.com/dogtopus/minipb)
    * délka zprávy se může za běhu změnit, ale struktura nikoli. 

---

# Přenos přes nespolehlivé linky

* Uvažujme systém, v němž vysílač periodicky posílá zprávy příjemci prostřednictvím komunikačního spojení. 

* Problém s rámováním zpráv vzniká, pokud je toto spojení nespolehlivé, což znamená, že při přenosu může dojít ke ztrátě nebo poškození dat. 

* V případě formátů ASCII s oddělovačem může přijímač, jakmile zjistí problém, zahazovat znaky, dokud nepřijme oddělovač, a pak čekat na kompletní zprávu.

* V případě binárních formátů je obecně nemožné provést opětovnou synchronizaci na souvislý tok dat. 
  * V případě pravidelných dávek dat lze použít časový limit. 
  * V opačném případě je nutná signalizace, kdy přijímač signalizuje vysílači, aby si vyžádal opakování přenosu.

---

# ujson a pickle

* Výhodou [ujson](https://docs.micropython.org/en/latest/library/json.html) je, že řetězce JSON mohou být akceptovány jazykem Python i jinými jazyky. 
* Nevýhodou je, že pouze podmnožinu objektových typů jazyka Python lze převést na legální řetězce JSON; jedná se o omezení [specifikace JSON](https://www.ecma-international.org/publications-and-standards/standards/ecma-404/).

<hr>

* Výhodou pickle je, že akceptuje jakýkoli objekt jazyka Python s výjimkou instancí uživatelsky definovaných tříd. 
* Extrémně jednoduchý zdrojový kód lze nalézt v oficiální knihovně. 
* Vytvořené řetězce jsou nekompatibilní s pickle CPythonu, ale lze je dekódovat v CPythonu pomocí dekodéru MicroPython. 

---

# Příklady

```python
import pickle
data = {1:'test', 2:1.414, 3: [11, 12, 13]}
s = pickle.dumps(data)
print('Human readable data:', s)
v = pickle.loads(s)
print('Decoded data (partial):', v[3])
```

```python
import ujson
data = {'1':'test', '2':1.414, '3': [11, 12, 13]}
s = ujson.dumps(data)
print('Human readable data:', s)
v = ujson.loads(s)
print('Decoded data (partial):', v['3'])
```

---

# Data proměnné délky

* V reálných aplikacích se data, a tedy i délka řetězce, za běhu mění. 
* Přijímací proces potřebuje vědět, kdy byl přijat celý řetězec.
* V praxi ujson a pickle nezahrnují do kódovaných řetězců znaky nového řádku. 
  * Pokud kódovaná data obsahují nový řádek, je v řetězci escapován.

<br>

```python
import ujson
data = {'1':b'test\nmore', '2':1.414, '3': [11, 12, 13]}
s = ujson.dumps(data)
print('Human readable data:', s)
v = ujson.loads(s)
print('Decoded data (partial):', v['1'])
```

---

# ustruct

* Binární formát [ustruct](https://docs.micropython.org/en/latest/library/struct.html) je efektivní, ale formát sekvence se za běhu nemůže změnit a musí být znám procesu dekódování. 

* Záznamy mají pevnou délku. Pokud mají být data uložena v binárním souboru s náhodným přístupem, znamená pevná velikost záznamu, že lze snadno vypočítat offset daného záznamu.

```python
import ustruct
fmt = 'iii'  # Record format: 3 signed ints
rlen = ustruct.calcsize(fmt)  # Record length
buf = bytearray(rlen)
with open('myfile', 'wb') as f:
    for x in range(100):
        y = x * x
        z = x * 10
        ustruct.pack_into(fmt, buf, 0, x, y, z)
        f.write(buf)
```

---

# MessagePack

* Z binárních formátů je tento formát nejjednodušší na použití a může být "náhradou" za ujson, protože podporuje stejné čtyři metody dump, dumps, load a loads.
* Aplikace může být zpočátku vyvíjena s protokolem ujson, později se protokol změní na MessagePack. 
* Vytvoření řetězce MessagePack lze provést pomocí:

```python
import umsgpack
obj = [1.23, 2.56, 89000]
msg = umsgpack.dumps(obj)  # msg is a bytes object
```

* Více v [dokumentaci](https://github.com/peterhinch/micropython-samples/blob/master/SERIALISATION.md).

---

# Protocol Buffers

* Jedná se o [standard](https://protobuf.dev/) společnosti Google popsaný např. v tomto [článku](https://en.wikipedia.org/wiki/Protocol_Buffers). 
* Záznamy mají proměnnou délku, přenáší se řetězce a čísla libovolné velikosti. 

* Implementace kompatibilní s MicroPythonem je [mikro implementace](https://github.com/dogtopus/minipb): 
  * soubory .proto nejsou podporovány, 
  * datový formát však deklaruje kompatibilitu s jinými platformami a jazyky.

* Schéma je tuple definující strukturu datového diktu. 
  * Každý prvek deklaruje klíč a jeho datový typ ve vnitřním tuplu. 
  * Prvky tohoto vnitřního tuplu jsou řetězce, přičemž prvek 0 definuje klíč pole. 
  * Následující prvky definují datový typ pole; ve většině případů je datový typ definován jediným řetězcem.

---

# Datové typy

* Popsány v [dokumentaci](https://github.com/dogtopus/minipb/wiki/Schema-Representations)

<br>

1. **'U'** Řetězec v kódování UTF8.
2. **'a'** Objekt bajtů.
3. **'b'** Logická hodnota.
4. **'f'** 32bitový float: obvyklé výchozí nastavení MicroPythonu.
5. **'z'** Int: celé číslo se znaménkem libovolné délky. Efektivně zakódováno pomocí důmyslného algoritmu.
6. **'d'** 64bitový float s dvojnásobnou přesností.
7. **'x'** Prázdné pole.

---

# Příklad

```python
import minipb

schema = (('value', 'z'),)  # Dict will hold a single integer
w = minipb.Wire(schema)

data = {'value': 0}
data['value'] = 150
tx = w.encode(data)
rx = w.decode(tx)  # received data
print(rx)
```

* Tento příklad zamlčuje skutečnost, že v reálné aplikaci se budou data měnit a délka přenášeného řetězce tx se bude měnit. Přijímací proces musí znát délku každého řetězce.

---

# Příklad vysílající strany 

```python
import minipb
schema = (('value', 'z'),
          ('float', 'f'),
          ('signed', 'z'),)
w = minipb.Wire(schema)
# Create a dict to hold the data
data = {'value': 0,
        'float': 0.0,
        'signed' : 0,}
while True:
    # Update values then encode and transmit them, e.g.
    # data['signed'] = get_signed_value()
    tx = w.encode(data)
    # Data lengths may change on each iteration
    # here we encode the length in a single byte
    dlen = len(tx).to_bytes(1, 'little')
    send(dlen)
    send(tx)
```

---

# Příklad přijímající strany

```python
import minipb
# schema must match transmitter. Typically both would import this.
schema = (('value', 'z'),
          ('float', 'f'),
          ('signed', 'z'),)

w = minipb.Wire(schema)
while True:
    dlen = receive(1)  # Data length stored in 1 byte
    data = receive(dlen)  # Retrieve actual data
    rx = w.decode(data)
    # Do something with the received dict
```

---

# V čem se dá ještě programovat (nejen RPi Pico)? 

---

# C/C++

* [C/C++ SDK](https://www.raspberrypi.com/documentation/microcontrollers/c_sdk.html)

```C
#include "pico/stdlib.h"

const uint LED_PIN = 25;

int main() {
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    while (1) {
        gpio_put(LED_PIN, 0);
        sleep_ms(250);
        gpio_put(LED_PIN, 1);
        sleep_ms(1000);
    }
}
```

---

# CircuitPython

```python
import time
import board
import digitalio

led = digitalio.DigitalInOut(board.LED)
led.direction = digitalio.Direction.OUTPUT

while True:
    led.value = True
    time.sleep(0.5)
    led.value = False
    time.sleep(0.5)
```

---

# Javascript

* Tiny Javascript runtime [Kaluma](https://kalumajs.org/), [github](https://github.com/kaluma-project/kaluma)
* Javascript engine (JerryScript)[https://jerryscript.net/]

```javascript
var led = 25;
pinMode(led, OUTPUT);

setInterval(() => {
  digitalToggle(led);
}, 1000);
```

---

# Arduino

* Komunitní [port](https://github.com/earlephilhower/arduino-pico) založený na C SDK, [dokumentace](https://arduino-pico.readthedocs.io/en/latest/)
* Oficiální port založený na [Arm Mbed](https://os.mbed.com/)


```Arduino
#define LED 25

void setup() {
  pinMode(LED, OUTPUT);
}

void loop() {
  digitalWrite(LED, HIGH);
  delay(1000);
  digitalWrite(LED, LOW);
  delay(1000);
}
```

---

# Rust

* [Rust](https://www.rust-lang.org/) podporuje RP2040 prostřednictvím [HAL](https://github.com/rp-rs/rp-hal)

* Existuje řada [tutoriálů](https://reltech.substack.com/p/getting-started-with-rust-on-a-raspberry) a [knih](https://docs.rust-embedded.org/book/intro/index.html), 

* Porovnání [Rust vs. C](https://kornel.ski/rust-c-speed)

* Love: [10 Reasons Not To Use Rust (The Whole Truth)](https://www.youtube.com/watch?v=ul9vyWuT8SU) 

* Hate: [Stop writing Rust](https://www.youtube.com/watch?v=Z3xPIYHKSoI)

---

```rust
#![no_std]
#![no_main]

use cortex_m_rt::entry;
use defmt::*;
use defmt_rtt as _;
use embedded_hal::digital::v2::OutputPin;
use embedded_time::fixed_point::FixedPoint;
use panic_probe as _;
use rp2040_hal as hal;

use hal::{
    clocks::{init_clocks_and_plls, Clock},
    pac,
    io::Sio,
    watchdog::Watchdog
};

#[link_section = ".boot2"]
#[used]
pub static BOOT2: [u8; 256] = rp2040_boot2::BOOT_LOADER_W25Q080;
```
--- 

```rust
#[entry]
fn main() -> ! {
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);

    let external_xtal_freq_hz = 12_000_000u32;
    let clocks = init_clocks_and_plls(
        external_xtal_freq_hz,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().integer());
```
---

```rust
    let pins = hal::gpio::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let mut led_pin = pins.gpio25.into_push_pull_output();

    loop {
        led_pin.set_high().unwrap();
        delay.delay_ms(500);
        led_pin.set_low().unwrap();
        delay.delay_ms(500);
    }
}
```
---

# Lua

```lua
gpio_pin = 25
pico.gpio_set_function (gpio_pin, GPIO_FUNC_SIO)
pico.gpio_set_dir (gpio_pin, GPIO_OUT)
while true do
  pico.gpio_put (gpio_pin, HIGH)
  pico.sleep_ms (300)
  pico.gpio_put (gpio_pin, LOW)
  pico.sleep_ms (300)
end
```

---

# Go

* Kompilátor [TinyGo](https://tinygo.org/), [getting started](https://tinygo.org/docs/reference/microcontrollers/pico/), YouTube [tutoriál](https://www.youtube.com/watch?v=Fl5eFIYU1Xg)

```go
package main

import (
    "machine"
    "time"
)

func main() {
    led := machine.LED
    led.Configure(machine.PinConfig{Mode: machine.PinOutput})
    for {
        led.Low()
        time.Sleep(time.Millisecond * 500)

        led.High()
        time.Sleep(time.Millisecond * 500)
    }
}
```
---

# Klasifikace IoT zařízení

## Vestavný systém
* vykonává omezený soubor specifických funkcí;
* často komunikuje se svým okolím.

## RT systém
* správnost systému závisí nejen na logických výsledcích, ale také na čase, za který jsou výsledky vytvořeny.

---

# Příklady systémů pracujících v reálném čase

* Real-time vestavné:
	* Systémy kritické z hlediska bezpečnosti: řízení jaderného reaktoru, řízení letu
	* GPS, MP3 přehrávač, mobilní telefon
* Real-time, ale ne vestavné:
  * Plaforma pro burzovní operace
  * Skype, Youtube, Netflix
* Vestavné, ale ne real-time:
  * Domácí termostat, zavlažovací systém
  * Pračka, lednička

---

# Charakteristiky real-time systémů

* Řízení událostí (reaktivní) vs. řízení podle času
* Požadavky na spolehlivost/odolnost proti poruchám (příklad: trojnásobná modulární redundance)
* Předvídatelnost
* Priority ve víceúlohových systémech

![center](figs/iot-1.png)

---

# Klasifikace RT systémů

* Hard RT: reakce na vstupy musí přijít v požadovaném termínu -  systémy řízení letů.
  * Real RT: navíc velmi krátká odezva, např. systém navádění raket
* Soft RT: termíny jsou důležité, ale systém bude správně fungovat i v případě, že budou termíny občas nedodrženy. Např. systém sběru dat.
* Firm RT: několik zmeškaných termínů nepovede selhání, ale více než několik zmeškaných termínů může vést k úplnému nebo katastrofickému selhání systému.

![](figs/iot-2.png)

---

# Klasifikace RT systémů

### Statické
* Lze předvídat časy příchodu úkolů
* Možnost statické analýzy (v době kompilace)
* Umožňuje dobré využití zdrojů (nízká doba nečinnosti procesorů)
### Dynamické
* Nepředvídatelné časy příjezdu
* Statická analýza (v době kompilace) je možná pouze pro jednoduché případy
* Využití procesoru se dramaticky mění - návrh tak, aby zvládl "nejhorší případ".
* Je třeba se vyvarovat příliš zjednodušujících předpokladů, např. předpokladu, že všechny úlohy jsou nezávislé, i když je to nepravděpodobné.

---

# Klasifikace RT systémů

### Periodické

* Každá úloha (nebo skupina úloh) se provádí opakovaně s určitou periodou.
* Umožňuje použití některých technik statické analýzy
* Odpovídá charakteristikám mnoha skutečných problémů
* Je možné mít úlohy s termíny menšími, rovnými nebo většími, než je jejich perioda - pozdější jsou obtížně zpracovatelné, vyskytuje se více souběžných instancí úloh

### Neperiodické (sporadické, asynchronní nebo reaktivní)
* Vytváří dynamickou situaci
* Časově omezené intervaly příchodu jsou snadněji zvládnutelné 
* Systémy s omezenými zdroji nezvládnou neomezené časové intervaly příchodu 
		
---

# Řídicí systémy

![w:550px center](figs/iot-5.svg)

<br>

* Rozhraní člověk-stroj: vstupní zařízení (klávesnice), a výstupní zařízení (displej).
* Přístrojové rozhraní: snímače a akční členy, převádějí fyz. signály na dig. data.
* Většina řídicích systémů je v tvrdém reálném čase
* Termíny jsou určeny řízeným objektem, tj. časovým chováním fyzikálního jevu (vstřikování paliva vs. ATM).

---

# Příklad řídicího systému

* Jednoduchý systém s jedním senzorem a jedním aktuátorem

<br>

![w:750px center](figs/iot-4.svg)

---

# Operační systém?

---

# Běh systému

* Úroveň úlohy, úroveň přerušení
* Kritické operace se musí provádět na úrovni přerušení (není dobré)
* Doba odezvy/časování závisí na celé smyčce
* Změna kódu ovlivňuje časování
* Jednoduché, levné systémy
	
![h:480px bg right](figs/iot-6.png)

---

# RT systémy

* Časové požadavky různých podnětů/odpovědí
  * architektura systému umožňovat rychlé přepínání mezi zpracovateli podnětů.
* Různé prioritám, neznámé pořadí a různé časové požadavky 
  * sekvenční smyčka obvykle nevyhovuje.

RT systémy se proto obvykle navrhují jako procesy spolupracující s jádrem reálného času, které tyto procesy řídí.
	
![h:380px bg right](figs/iot-7.png)

---

# Plánovací strategie

* **Nepreemptivní plánování** - jakmile je proces naplánován k provedení, běží až do dokončení nebo dokud není z nějakého důvodu zablokován (např. čekání na vstup/výstup).
* **Preemptivní plánování** - Provádění prováděných procesů může být zastaveno, pokud proces s vyšší prioritou vyžaduje obsluhu.

### Plánovací algoritmy

* [Round-robin](https://cs.wikipedia.org/wiki/Round-robin\_scheduling) - běžícímu procesu přiděluje kvantum času, po jeho uběhnutí je proces odstaven, předpokládá se konstatní priorita

* [Rate monotonic](https://cs.wikipedia.org/wiki/Rate\_monotonic\_scheduling) - statické přidělování priorit
		
* [Earliest deadline first](https://cs.wikipedia.org/wiki/Earliest\_deadline\_first) - zpracování úloh probíhá na základě mezní doby platnosti procesu
			

---

# RT operační systémy - RTOS

* RTOS jsou specializované operační systémy, které řídí procesy v RTS.

* Odpovídají za správu procesů a přidělování zdrojů (procesoru a paměti).

* Komponenty
	* **Real-time clock** poskytuje informace pro plánování procesů
  * **Interrupt handler** spravuje nepravidelné žádosti o obsluhu
  * **Scheduler** vybere další proces, který má být spuštěn
  * **Resource manager**  alokuje paměť a zdroje (procesor, vlákno, ...) 
  * **Dispatcher** spustí proces

---

# Obsluha přerušení v RTOS

* Ovládání se automaticky přenese do předem určeného místa v paměti.

* Toto místo obsahuje instrukci pro skok do rutiny obsluhy přerušení.

* Další přerušení (od stejného zdroje) jsou zakázána

* Přerušení je obslouženo a řízení je vráceno přerušenému procesu.

* Obslužné rutiny přerušení MUSÍ být krátké, jednoduché a rychlé.

---

# Parametry pro výběr vhodného operačního systému

* **Režie** minimální požadavky na paměť, spotřebu a výkon.
* **Přenositelnost** middleware je nezávislý na hardware, obvykle je na různé platformy a rozhraní přenášen pomocí BSP (Board Support Package)
* **Modularita** pevné jádro a další funkce by měly být přidány ve formě doplňků. OS je pak možné přizpůsobit aplikaci a zredukovat potřebné zdroje.
* **Konektivita** podpora různých protokolů, např. Ethernet, Wi-Fi, BLE, IEEE 802.15.4
* **Škálovatelnost** škálovatelnost pro jakýkoli typ zařízení -- vývojáři a integrátoři budou znát pouze jeden OS pro uzly i brány.
* **Spolehlivost** dlouhodobý běh aplikace bez selhání, certifikace pro určité aplikace. 
* **Bezpečnost** bezpečné spouštění, podpora SSL a ovladačů pro šifrování.

---

# Spolehlivost -- certifikace

* **DO-178B** pro systémy avioniky
* **IEC 61508** pro průmyslové řídicí systémy
* **ISO 62304** pro zdravotnické zařízení
* **SIL3 / SIL4** pro dopravu a jaderné systémy (Safety integrity level)

## MISRA

* Motor Industry Software Reliability Association 
* Sada doporučení pro bezpečné programy C a C++
* Cílem je zajistit bezpečnost a robustustnost implementace
* Gudelines nejsou volně dostupné

---

# Operační systém RIOT

* Bezplatný operační systém s otevřeným zdrojovým kódem (LGPLv2.1)
* Programování je v jazyku C/C++ nebo Rust
* Podporuje standardní nástroje jako jsou gcc, gdb nebo valgrid.
* Architektury: AVR, ARM7, Cortex-M0, Cortex-M0 +, Cortex-M3, Cortex-M4, Cortex-M7, ESP8266, MIPS32, MSP430, PIC32, x86.
* Desky: Airfy Beacon, Arduino Due, Arduino Mega 2560, Arduino Zero, Atmel samr21-Xplained Pro, f4vi, mbed NXP LPC1768, Micro::bit, Nordic nrf51822 (DevKit), Nordic nrf52840 (DevKit), Nucleo desky (téměř všechny) a mnoho dalších.

Web: https://www.riot-os.org/

---

# Příklad programu v RIOT OS

```c
gpio_t pin_out = GPIO_PIN(PORT_B, 5);
if (gpio_init(pin_out, GPIO_OUT)) {
    printf("Error to initialize GPIO_PIN(%d %d)\n", PORT_B, 5);
    return -1;
}

while(1)
{
  printf("Set pin to HIGH\n");
  gpio_set(pin_out);
  xtimer_sleep(2);

  printf("Set pin to LOW\n");
  gpio_clear(pin_out);
  xtimer_sleep(2);
}
```

Web: https://www.hackster.io/ichatz/control-external-led-using-riot-os-b626da

---

# Mongoose OS

* Framework pro vývoj firmwaru internetu věcí (Apache Licence 2.0 nebo Enterprise)

* Cílem je komplexní řešení pro vývoj a správu
* Nízkopříkonové MCU: ESP32, ESP8266, TI CC3200, STM32
* Cloudové integrace: AWS, Google, Azure, IBM Watson (komerční licence)
* K dispozici je Dashboard [mDash](https://mdash.net/home/) (Apache 2.0)
  * stav připojených zařízení - online / offline, verze firmwaru, doba provozu atd. 
  * aktualizace firmwaru OTA,  
* K dispozici je také mobilní aplikace pro iOS i Android.

Web: https://mongoose-os.com/

---

# Mongoose OS
## Příklad čtení dat ze senzoru DHT22

```c
#include "mgos.h"
#include "mgos_dht.h"

static void timer_cb(void *dht) {
  LOG(LL_INFO, ("Temperature: %lf", mgos_dht_get_temp(dht)));
}

enum mgos_app_init_result mgos_app_init(void) {
  struct mgos_dht *dht = mgos_dht_create(mgos_sys_config_get_app_pin(), DHT22);
  mgos_set_timer(1000, true, timer_cb, dht);
  return MGOS_APP_INIT_SUCCESS;
}
```

---

# Contiki OS

* Operační systém s otevřeným zdrojovým kódem (BSD licence)

* Poskytuje multitasking, jádro 10kB RAM + 30kB ROM, jazyk C

* Simulátor Cooja umožňuje emulovat celou Contiki síť

Web: https://github.com/contiki-ng/contiki-ng/wiki

---

# Projekt Zephyr

* Malý škálovatelný RTOS, původně pod patronací Linux Foundation (Apache licence)

* Jádro Zephyr je odvozeno od komerčního VxWorks Microkernel Profile.

* MCU: https://docs.zephyrproject.org/latest/boards/index.html

* Raspberry Pi Pico [tutorial](https://www.mrgreensworkshop.com/posts/2022-06-10-raspberry-pi-pico-zephyr-os-part-2) 

Web: https://zephyrproject.org/

## Blinky LED

* Zdrojový [kód](https://github.com/zephyrproject-rtos/zephyr/blob/main/samples/basic/blinky/src/main.c)
* Konfigurace specifického [boardu](https://docs.zephyrproject.org/latest/samples/basic/blinky/README.html#adding-board-support)


---

# Nucleus

* OS od divize Embedded Software společnosti Mentor Graphics

* Honeywell: systém varování před přiblížením k zemi v aplikaci pro letecký průmysl.

* Garmin: Avionics Navigator CNX80

* ZOLL: automatizovaný externí defibrilátor AED Plus

* MCU: https://www.mentor.com/embedded-software/nucleus/processor-support

<hr>

Update 2023: RTOS převzal [Siemens](https://www.plm.automation.siemens.com/global/en/products/embedded/nucleus-rtos.html)

---

# Mbed OS

* RTOS určený primárně pro procesory ARM (licence Apache 2.0)

* Dostupný zdrojový kód: https://github.com/ARMmbed/mbed-os
* Podporuje BLE, NFC, RFID, LoRa, 6LoWPAN-ND, Thread, Wi-SUN, Ethernet, Wi-Fi, LPWAN
* Propojení s platformou [Pelion IoT](https://pelion.com/)
* Nástroje: [Mbed CLI](https://os.mbed.com/docs/mbed-os/v6.15/quick-start/build-with-mbed-cli.html), [Mbed Online Compiler](https://os.mbed.com/docs/mbed-os/v6.15/quick-start/build-with-the-online-compiler.html), [Mbed Studio](https://os.mbed.com/docs/mbed-studio/current/getting-started/index.html)
	
* [Desky](https://os.mbed.com/platforms/), [Komponenty](https://os.mbed.com/components/)

Web: https://os.mbed.com/