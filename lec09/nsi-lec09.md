---
marp: true
paginate: true

style: |
    img[alt~="right"] {
      position: relative;
      top: -120px;
      float: right;
    }
    img[alt~="center"] {
      display: block;
      margin: 0 auto;
      padding-top: 20px; 
    }
    section{
      
    }
    strong {color: #455a64; font-weight: 600;}

    div.twocols {
        margin-top: 35px;
        column-count: 2;
    }
    div.twocols p:first-child,
    div.twocols h1:first-child,
    div.twocols h2:first-child,
    div.twocols ul:first-child,
    div.twocols ul li:first-child,
    div.twocols ul li p:first-child {
        margin-top: 0 !important;
    }
    div.twocols p.break {
        break-before: column;
        margin-top: 0;
    }
---

<!-- _paginate: false -->

# Návrh systémů IoT

## 9. Virtualizace a cloud

Stanislav Vítek
Katedra radioelektroniky
České vysoké učení technické v Praze

<!--
---

# Obsah přednášky

-->
---

# Zvyšování a optimalizace výpočetního výkonu

* Clustering
  * výkon, stabilita, vysoká dostupnost prostřeků
  * problém s vytížeností, nákladnost

* Virtualizace
  * sdílení prostředků a výkonu, vyvažování

* Cloud
  * virtuální hw platforma pro provozování virtuálních serverů a služeb
  * škálovatelnost, elasticita, vysoká dostupnost 
  * ekonomika - datová centra, poskytovatelé

---

# Virtualizace ve výpočetních infrastrukturách

* Virtuální paměť
  * 1962; běžně se používá od 70. let (IBM S/370 a mnoho dalších)
  * Vždy implementována hardwarově, řízena operačním systémem
* Virtuální stroje
  * 1972 (IBM S/370), opuštěno před rokem 1990
  * Oživeno v roce 1999 (VMWare na Intel/AMD x86)
  * Původně implementovány čistě SW (ale vyvinut společně s HW v IBM S/370)
  * Specifická hardwarová podpora v procesorech Intel/AMD od roku 2005
* Virtuální disky
  * 1974; Unix - ovladače blokových zařízení (RAM-disky atd.)
  * Výkonné verze implementované ve specializovaném HW (řadiče RAID)

Dnes: virtuální síťové karty, VLAN, VPN, ...

---

# Motivační příklad - Webhosting

* Statické stránky
  * 1 web = 1 adresář
  * 1 proces (Apache) pro všechny

* Dynamické stránky
  * potenciálně nebezpečný kód
  * 1 web = 1 proces (Apache+PHP, Flask+Python)
  * 1 počítač pro všechny

* Uživatelské systémy
  * weby vyžadují odlišné konfigurace
  * 1 web = 1 počítač
  * bez virtualizace zbytečně drahé

---

# Webhosting a virtualizace z rychlíku

![h:500px center](figs/iot-01.drawio.svg)

---

# Webhosting a virtualizace podrobněji

![h:500px center](figs/iot-02.drawio.svg)

---

# Motivace k virtualizaci - pronájem služby

* Nájemce - osoba/společnost, která využívá soubor služeb.
  * Odlišný od vlastníka hardwaru
  * zcela jiná (právnická) osoba (zákazník), nebo organizační jednotka využívající služby dodávané oddělením IT apod.

* Prostředí s více nájemci
  * Hardwarové prostředky sdílené mezi více nájemci
  * Nájemci nemohou sdílet prostředky dobrovolně
    * Obvykle se navzájem neznají
    * Nechtějí vyjednávat o prostředcích
    * Jejich software nelze dostatečně přizpůsobit sdílení prostředků

---

# Motivace k virtualizaci - pronájem služby

* Granularita sdílení více nájemců
  * Fyzický počítač je často příliš velký
    * Vyrovnávání zátěže může vyžadovat fragmenty výkonu fyzického počítače
  * Je příliš obtížné přeřadit fyzický počítač jinému nájemci
    * I v případě automatizace může takové přeřazení trvat hodiny

---

# SW závislostí

**Software není jeden soubor nebo složka.**

* Spustitelné soubory jsou propojeny s dynamicky načítanými knihovnami.
  * Odkazuje se na ně krátkým názvem, například "libcrt.so".
* Aplikace je často rozdělena do komunikujících procesů
  * Často proto, že tvůrci nejsou schopni propojit jednotlivé části dohromady
  * Propojené pojmenovanými rourami nebo IP sokety, identifikované názvy souborů, čísly portů
* Existují zdroje, konfigurace, data, multimédia, ...
  * Někde uloženy jako soubory, identifikované relativními/absolutními názvy souborů
*   Různé systémy mají protichůdné konvence
* Všechny složky musí mít stejnou nebo kompatibilní verzi

---

# SW závislostí

**Koexistence dvou verzí téhož softwaru**

* Nutné, pokud software A a B vyžadují různé verze softwaru C
A a B musí být nakonfigurovány tak, aby pod stejným názvem našly různé verze C

* Příprava takových konfigurací je obtížná
  * Takové konfigurace by se odchýlily od systémových konvencí (jako je /etc/*).
  * Složité konfigurace mohou snížit výkon (např. dlouhá LD_LIBRARY_PATH)
  * Často neexistuje vůbec žádná možnost konfigurace

---

# Řešení? Virtualizace!

* Hardwarový stroj může hostit více virtuálních strojů
* Virtuální stroje mohou migrovat mezi hardwarovými stroji
* Virtuální stroje lze snadno zastavit, vytvořit, zničit, ...

<!--
* virtuální počítače
* webové stránky
* cloudové služby, webové/pracovní role
* kontejnery a mikroslužby, orchestrace
* bezserverové výpočty
* mobilní služby
* vysoce výkonné výpočty
* správa cloudu, monitorování
-->

---

# Execution / deployment models

* způsoby nasazení a běhu 'kódu' v cloudu

* úroveň poskytovaných služeb
  * pro provozování vlastních aplikací

* IaaS, PaaS
  * nemusím se o nic starat vs. můžu cokoliv

* architektura služeb
  * granularita výpočetních jednotek, škálovatelnost

---

# Distribuční modely IaaS, Paas a SaaS

![h:550px center](figs/Management2.png)

---

# Virtual machine

* **vlastní image / gallery** + VM extensions
* **další cloudové služby** - storage, networking, AI, ...
* **pay-per-use, pay-per-config** - memory, processors, disk space, ...
* **administrace** - portal / scripting console / API
* **běžící VM**
  * virtuální disky - OS, data
  * typicky BLOB (Binary Large Objects)
* **využití**
  * development / test environment / běh aplikací / provoz služeb
  * rozšíření datacentra / disaster recovery

---

# Virtual machine - IaaS

* Infrastructure-as-a-Service
* fault tolerance, monitoring
* aktualizace gallery
* SLA - Service Level Agreement
  * 99.9x% dostupnost ≈ jednotky hodin / rok
  * hardware failure - disk, CPU, memory
  * datacenter failures - network / power
  * hw upgrade, sw maintenance

---

# Virtual machine - Grouping

* load balancing
  * rozložení požadavků
* availability set
  * rozložení na různé uzly
  * no single point of failure
  * maintenance, upgrade, failure
  * rolling updates
* komunikace

---

# Web Sites / Web Apps

* webová aplikace na různých platformách
  * jedno z nejčastějších využití cloudových infrastruktur
* ideální podmínky pro nasazení v cloudu
  * nepredikovatelné škálovatelnost
* lze i pomocí virtual machines
  * zbytečně složité
  * nutná vlastní instalace, konfigurace a údržba

---

# Web Sites / Web Apps - PaaS

* Platform-as-a-Service, PaaS
  * předkonfigurované instalace
  * kompletní framework - OS, db, web server, knihovny, ...
  * administrace, aktualizace a údržba komponent
  * vývojář jen nakopíruje html/php/js/...
* dynamické přidávání instancí, load balancing
  * automatické škálování - dle rozvrhu, používání, nebo dosažením kvót
* různé úrovně izolace - shared / private VM
* různé frameworky, jazyky a db - JAVA, .Net, PHP, Node.js, *-SQL, Drupal, WordPress, ...
* běh scriptů / jobů
  * on demand / nepřetržitě / schedule / fronty

---

# Web/Worker Roles - Cloud Services

* poskytování škálovatelných SaaS služeb
  * **WS** - omezený přístup, nelze instalovat cokoliv, nejsou dostupné všechny služby
  * **VM** - plný přístup, ale nutnost administrace a údržby, samo neškáluje
* aplikační model s větším počtem rolí 
* web / worker roles
  * front-end: GUI, http
  * back end: výpočty, aplikační logika, data processing
* vhodné pro vícevrstvé škálovatelné aplikace

---

# Web/Worker Roles

* jedna adresa, více web/worker rolí
  * vícevrstvé aplikace
  * automatický load balancing a availability set
* administrativní přístup do VM
  * instalace potřebného software
  * propojení cloudové aplikace s privátními uzly
* vývojové a produkční prostředí (staging area)
  * jednoduchý vývoj/testování a přechod na novou verzi
  * upgrade jen pro část provozu
* monitorování hw/sw
  * agent uvnitř web/worker role

---

# Vlastnosti cloudů

### Sdílení systému (multitenancy)
* jeden prostředek (server, data, síť) využívá více uživatelů současně
* virtualizované zdroje jsou logicky odděleny
  * nelze přistupovat k cizím zdrojům

### Thin provisioning
* virtuální alokace prostoru na úložišti dat
* klientu je zdánlivě vyhrazena požadovaná kapacita
  * ve skutečnosti ji mohou až do doby využití používat jiné systémy
* různé úrovně záruky dostupnosti - SLA

---

# Vlastnosti cloudů

### Škálovatelnost a elasticita (scalability, elasticity)
* změna výkonu podle potřeb klienta
* služby jsou účtovány podle skutečného využití

### Spolehlivost a dostupnost (reliability, availability)
* záložní systémy na různých úrovních (servery, infrastruktura, datová centra)
* software, který za provozu zajistí rychlé nahrazení nefunkční části systému

### Aktualizovanost (up-to-date)
* software je automaticky aktualizovaný, uživatel nemusí zasahovat

---

# Druhy cloudů

* Veřejný cloud (Public cloud)
  * poskytování služeb (IaaS, PaaS, SaaS) třetí stranou - nejčastější typ
  * zajištěna vysoká škálovatelnost a účtování podle využívaných zdrojů
* Soukromý cloud (Private cloud)
  * infrastruktura poskytující služby pouze jedné organizaci
  * schopnost účtování jednotlivým složkám organizace
* Komunitní cloud (Community cloud)
  * cloud využívaný komunitou - spolupracující firmy, projekt apod.
* Hybridní cloud (Hybrid cloud)
  * cloud složený z více různých cloudů, např. několika veřejných a soukromého
  * cloud interoperability - Sky Computing

---

# Kontejnery

* sdílení systémových souborů, paměti, ...
* izolace namespace, kontrola prostředků
* komodizace cloud platforem
  * prevence vendor lock-in
* **Dev** (development)
  * kód, knihovny, konfigurace, služby, data
* **Ops** (operations)
  * monitorování, síť, logování, startování /  přesouvání / vypínání kontejnerů
* Docker, Rocket, ... 

---

# Kontejnery

### Image
* r/o obraz souborového systému
* distribuční balíček
* zapouzdřuje metadata
* síťové porty, připojené části FS, ...

### Kontejner
* běhová r/w instance image
* obsahuje běžící procesy (často jeden proces)
* izolace
* commit - lze vytvořit nový image

---

![](figs/life-cycle-containerized-apps-docker-cli.png)

---

# Kontejner vs. Virtual Machine

## +
+ menší režie, rychlejší start
+ snadný deployment, škálovatelnost

## -
+ menší stupeň izolace
+ bezpečnost

---

# Kontejner vs. Virtual Machine

![h:500px center](figs/iot-03.drawio.svg)

---

# Microservices

* návrhový vzor
* dekompozice na malé samostatné služby
  * independent, self-contained, bounded contexts
* snadná flexibilita a cílená škálovatelnost
* efektivní implementace pomocí kontejnerů
* stateless / stateful  ↬
* př.: protocol gateways, user profiles, shopping carts, inventory processing, queues, caches, ...

---

# Container Management & Orchestration

* tisíce kontejnerů!

* plánování
  * job scheduling
  * resource scheduling
* automatizovaný výběr hostitele (host allocation)
* vyvažování výkonu (load balancing)
* zajištění dostupnosti (availability sets)
* health checking & logging, service restarting
* automatická replikace služeb

---

# Google Borg - Cluster Management System

* od 2005 (od 2013 Google Omega), 100k jobs, 10k nodes 
* Production / Non-Prod jobs - priority
* Borgmaster - 5x replicated - Paxos
* failure detection, relibility, availability

**Nástroje**
* naming, service discovery, load balancing
* horizontal/vertical autoscaling
* rollout tools
* deployment and configuration, workflow tools
* multijob pipelines with interdependencies between the stages 
* monitoring tools, gathering, aggregating and presentation, alert triggers

---

# Google Borg - Cluster Management System

![h:550px center](figs/iot-04.drawio.svg)

---

# Konsensus & replikovaný konečný automat

* Konsensuální algoritmus - dosažení shody většiny uzlů
  * 5 serverů může pokračovat i když 2 servery havarují
  * větší počet havárií ⇝ zastavení algortimu

* Typické využití: replikovaný konečný automat
  * každý uzel zná přechodové funkce, udržuje stav (automatu) a log příkazů
  * jeden krok: distribuovaný konsensus o následujícím příkazu
  * každý uzel provede stejnou sekvenci příkazů ⇝ stejná posloupnost stavů

* Z pohledu klienta: komunikace s jedním spolehlivým stavovým automatem

---

# Konsensus & replikovaný konečný automat
<br/>
<br/>

![w:1000px center](figs/iot-05.drawio.svg)

---

# Kubernetes

* open source od 2014
* první release 2015
  * Google Container Engine, Azure Container Services, AWS EC2 Container Services for Kub.
* application deployment, scheduling, updating, maintenance, scaling
* aktivně spravuje kontejnery
  * stav clusteru stále odpovídá specifikaci
* pod
  * množina kontejnerů alokovaných vždy na stejný uzel
* rolling updates
  * postupná aktualizace replik bez přerušení dostupnosti

---

# Kubernetes

![w:750 center](figs/iot-06.svg)

---

# Azure

* Service Fabric - API
  * development since 2008
  * open source since 2018
    * Windows, .Net ⇝ Linux, ... 
* Containers
* Reliable Stateful/Stateless Services
* N consistent copies (quorum) - replication and local persistence
* Reliable Actors
  * stateless & stateful actor objects, simplified single-threaded programming model

---

# Azure

* Guest Executables
  * any exe / language / prog model
  * packaged as application - versioning, upgrade, monitoring, health, etc.

* Cloud Services ⇝ Service Fabric
  * CS: code bound to a VM instance (Web/Worker Role)
  * SF: code bound to SF - abstract layer
    * faster (re)boot, dense hosting, multiplatform (Win/Lx), management
* Orchestration
  * Azure Container Service, Kubernetes (AKS), Docker Swarm, Apache Mesos, Deis

---

# Serverless computing

**No infrastructure, no OS, no installation**

* Funkce jako služba ⇝ stačí napsat funkci
  * žádné servery, které by bylo třeba zajišťovat nebo spravovat
  * automatické nasazení, spuštění, vypnutí, škálování
  * inherentní dostupnost a odolnost proti chybám
  * žádné poplatky za nevyužitou kapacitu
* Řízení událostí
  * žádná perzistence, žádný vnitřní stav
  * externí perzistentní úložiště
* Vazby na jiné cloudové služby
  * JavaScript, C#, Python, Go, Ruby, Java, PHP, PowerShell, ..., ...
  * sandbox / bytecode

---

# Serverless Computing - Use Cases

* timer-based processing
* ServiceBus / EventHub / SaaS event processing
* serverless web application
* serverless mobile backend
* real-time stream processing
* real-time chatbot messaging

---

# Serverless Computing - Use Cases

![h:550px center](figs/iot-1!.png)

---

# Serverless Computing - Triggery

![h:450px center](figs/iot-12.png)

---

# Serverless Computing - Triggery

![h:450px center](figs/iot-13.png)

---

# CDN - Content Delivery Nwtwork

* uživatel si vyžádá soubor
* DNS směruje požadavek na nejbližší místo POP (Point-of-Presence).
* pokud POP nemá soubor v mezipaměti, vyžádá si jeho původce
* původce vrátí soubor včetně doby do konce životnosti (TTL).
* POP si soubor uloží do mezipaměti (do doby TTL)
* další uživatelé mohou požádat o stejný soubor - uložený v mezipaměti

### Na světě řada CDN uzlů
